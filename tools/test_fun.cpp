#include "ThermoFun/ThermoFun.h"
using namespace std;

int main()
{
  // Create the engine object using a database file in JSON format
  // Examples of such files can be found in /Resources/databases/ folder or can be
  // retrieved from ThermoHub database using ThermoHubClient
  ThermoFun::ThermoEngine engine("pHtitr.FUN.json");

  double T       = 398.15; double P = 1e7; // in K and Pa

  auto propAl    = engine.thermoPropertiesSubstance(T, P, "quartz");

  // extracting values from results for the Gibbs energy
  double G0      = propAl.gibbs_energy.val; // value
  double G0dT    = propAl.gibbs_energy.ddt; // derivative with T = -S0
  double G0error = propAl.gibbs_energy.err; // propagated error

  std::cout << G0 << " "<<G0dT << " "<< G0error<< std::endl;
  return 0;
}
